//
//  ViewController.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var searchTextField: UITextField!
    
    @IBOutlet var searchOnWebButton: UIButton!
    
    @IBOutlet var searchLocallyButton: UIButton!
    
    var sendState = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        [searchTextField, searchOnWebButton, searchLocallyButton].forEach { (v) in
            v!.layer.cornerRadius = v!.frame.height / 2
        }
    }

    @IBAction func searchButtonDidClick(_ sender: UIButton) {
        self.sendState = 0
        toResult()
    }
 
    @IBAction func searchLocallyDidClick(_ sender: UIButton) {
        self.sendState = 1
        toResult()
    }
    
    
    /*
 
     leads to result page and setting the page as new.
     */
    func toResult(){
        self.performSegue(withIdentifier: Constants.segueFromSearchToResults, sender: self)
        self.searchTextField.endEditing(true)
        self.searchTextField.text = ""
        
    }
    // MARK: PREPARE FOR SEGUE
    
    // passing the query string to the result controller and setting its state
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueFromSearchToResults{
            if let dest = segue.destination as? SearchResultsViewController {
                dest.textToSearch = self.searchTextField.text?.lowercased() ?? ""
                dest.currentState = self.sendState == 0 ? .webSearch : .localSearch
            }
        }
    }
    
}

