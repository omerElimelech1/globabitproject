//
//  SearchResultsViewController.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import UIKit
import CoreData
import SkeletonView

class SearchResultsViewController: UIViewController {

    
    enum state {
        case webSearch
        case localSearch
    }
    
    var currentState : state = .webSearch
    
    @IBOutlet var tableView: UITableView!
    
    var textToSearch: String!
    
    var geoNames : [Geoname]?
    
    lazy var handler = RequestHandler()
    
    //MARK: CORE DATA PROPERTIES
    lazy var appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var managedContext = appDelegate.persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: Constants.searchResultCell, bundle: nil), forCellReuseIdentifier: Constants.searchResultCellRid)
        tableView.delegate = self
        tableView.dataSource = self
        
        switch self.currentState {
        case .webSearch:
            self.fetchDataWithKeyword(keyword: self.textToSearch)
        case .localSearch:
            self.fetchDataFromCoreDataWithKeyword(keyword: self.textToSearch)
        }
        
        
        
    }
    
    //MARK: DIDLAYOUTSUBVIEWS, SHOWS SKELETON IF CURRENT STATE = WEB SEARCH
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        switch self.currentState {
        case .webSearch:
            let gradient = SkeletonGradient(baseColor: UIColor.lightGray)
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            tableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        case .localSearch:
            return
        }
        
        
    }
    
    /*
     
     fetches data from server and update tableview.
     
     @param: keyword
    
     */
    func fetchDataWithKeyword(keyword: String){
        
        handler.getDataFromWikipediaBy(Keyword: keyword) { (result) in
            self.geoNames = result
            self.reloadTableView()
            if !self.isEntityExists(keyword: self.textToSearch){
                self.save(keyword: self.textToSearch, geonames: result)
            }
            
        }
        
        
    }
    func reloadTableView(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        self.tableView.hideSkeleton()

    }

}






