//
//  SearchResultTVDelegate.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import Foundation
import UIKit


extension SearchResultsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.searchResultCellRid, for: indexPath) as? SearchResultCell else { return UITableViewCell()}
        
        cell.geoName = self.geoNames?[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.geoNames?.count ?? 0
    }
    
    
}
