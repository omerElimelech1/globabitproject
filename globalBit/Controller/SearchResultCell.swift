//
//  SearchResultCell.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {
    
    
    @IBOutlet var thumbnailImageVIew: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    var geoName: Geoname? {
        didSet{
            if let urlString = geoName?.thunbnailURL{
                self.thumbnailImageVIew.cacheImage(urlString: urlString)
            }
            self.titleLabel.text = geoName?.titleText
            self.descriptionLabel.text = geoName?.descriptionText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
