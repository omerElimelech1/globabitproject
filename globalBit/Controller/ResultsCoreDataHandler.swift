//
//  ResultsCoreDataHandler.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import Foundation
import CoreData
import UIKit


extension SearchResultsViewController {
    
    /*
     this function saves the data to the local database
     @param: keyword
     @param: geoname (of type Geoname)
     
     @returns error when did not save successfuly
     */
    func save(keyword: String, geonames: [Geoname]) {
        let entity =
            NSEntityDescription.entity(forEntityName: Constants.entityName,
                                       in: managedContext)!
        
        
        geoNames?.forEach({ (geoname) in
            let entityObj = NSManagedObject(entity: entity,
                                            insertInto: managedContext)
            entityObj.setValue(geoname.thunbnailURL, forKeyPath: Constants.thumbnailUrl)
            entityObj.setValue(geoname.titleText, forKeyPath: Constants.title)
            entityObj.setValue(geoname.descriptionText, forKeyPath: Constants.summary)
            entityObj.setValue(keyword, forKeyPath: Constants.keyword)
        })
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    
    /*
     this function fetches data from the local database and filters by keyword, creates new array of Geoname and updates the tableview
     
     @param keyword     
     */
    func fetchDataFromCoreDataWithKeyword(keyword: String){
        var geoNames = [Geoname]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        request.predicate = NSPredicate(format: "\(Constants.keyword) = %@", keyword)
        request.returnsObjectsAsFaults = false
        do {
          
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
                let thumb = data.value(forKey: Constants.thumbnailUrl) as? String
                let title = data.value(forKey: Constants.title) as? String
                let summary = data.value(forKey: Constants.summary) as? String
                let geoName = Geoname(thumb: thumb, title: title, description: summary)
                geoNames.append(geoName)
            }
        } catch {
            
            print("Failed")
        }
        
        self.geoNames = geoNames
        self.reloadTableView()
        
    }
    
    func isEntityExists(keyword: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        request.predicate = NSPredicate(format: "\(Constants.keyword) = %@", keyword)
        request.returnsObjectsAsFaults = false
        
        var entitiesCount = 0
        
        do {
            entitiesCount = try managedContext.count(for: request)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return entitiesCount > 0
    }


}
