//
//  Geoname.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import Foundation


struct Geoname {
    
    let thunbnailURL: String?
    let titleText: String?
    let descriptionText: String?
    
    init(thumb: String? = nil, title: String? = nil, description: String? = nil) {
        self.thunbnailURL = thumb
        self.titleText = title
        self.descriptionText = description
    }
}
