//
//  Constants.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import Foundation


struct Constants {
    
    static let searchResultCell = "SearchResultCell"
    
    static let searchResultCellRid = "SearchResultCellRid"
    
    static let segueFromSearchToResults = "toResults"
    
    static let thumbnailUrl = "thumbnailUrl"
    
    static let title = "title"
    
    static let summary = "summary"
    
    static let keyword = "keyword"
    
    static let entityName = "Geonames"
    
    static let endPoint = "http://api.geonames.org/wikipediaSearchJSON"
    
    static let geoNameUsername = "tingz"
}
