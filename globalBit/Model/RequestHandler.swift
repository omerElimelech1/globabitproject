//
//  RequestHandler.swift
//  globalBit
//
//  Created by Omer Elimelech on 3/6/19.
//  Copyright © 2019 Omer Elimelech. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RequestHandler {
    
    func getDataFromWikipediaBy(Keyword keyword: String, complation: @escaping (_ result: [Geoname]) -> Void){
        let maxRows = 10
        var arr = [Geoname]()
        let params: Parameters = [
            "q" : keyword,
            "maxRows" : maxRows,
            "username" : Constants.geoNameUsername
        ]
        Alamofire.request(Constants.endPoint, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                        switch response.result {
                        case .success(let value):
                            let json = JSON(value)
                            if let geonamesarr = json["geonames"].array{
                                for geoname in geonamesarr{
                                    let summary = geoname["summary"].string
                                    let title = geoname["title"].string
                                    let thumbUrl = geoname["thumbnailImg"].string
                                    let g = Geoname(thumb: thumbUrl, title: title, description: summary)
                                    arr.append(g)
                                }
                            }
                          
                            complation(arr)
                            
                        case .failure(let error):
                            print(error)
                        }
        }
        
        
    }
    
    
}


